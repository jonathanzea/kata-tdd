package com.zea.tdd

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test

class DollarTest {

    @Test
    fun `a money can be compared`() {
        assertThat(Money.dollar(5) == (Money.dollar(5)), equalTo(true))
        assertThat(Money.dollar(5) == (Money.dollar(7)), equalTo(false))
        assertThat(Money.frank(5) == (Money.dollar(6)), equalTo(false))
    }

    @Test
    fun `a money can be multiplied`() {
        val five = Money.frank(5)
        assertThat(Money.frank(10), equalTo(five.times(2)))
        assertThat(Money.frank(15), equalTo(five.times(3)))
    }

    @Test
    fun `a money gets constructed with the appropriate currency`() {
        assertThat(Money.dollar(1).currency == "USD", equalTo(true))
        assertThat(Money.frank(1).currency == "CHF", equalTo(true))
    }

    @Test
    fun `a money dollar can be summed`() {
        val sum = Money.dollar(5).plus(Money.dollar(5))
        assertThat(Money.dollar(10), equalTo((sum as Sum).reduce(Bank(), "USD")))
    }

    @Test
    fun `a money summed returns a sum expression`() {
        val five = Money.dollar(5)
        val result: Expression = five.plus(five)
        val sum: Sum = result as Sum
        assertThat(five, equalTo(sum.augend))
        assertThat(five, equalTo(sum.addend))
    }

    @Test
    fun `a bank can reduced a sum`() {
        val sum: Expression = Sum(Money.dollar(3), Money.dollar(4))
        val bank = Bank()
        val result: Money = bank.reduce(sum, "USD")
        assertThat(Money.dollar(7), equalTo(result))
    }

    @Test
    fun `a bank can reduce single moneys`() {
        val bank = Bank()
        val result = bank.reduce(Money.dollar(1), "USD")
        assertThat(Money.dollar(1), equalTo(result))
    }

    @Test
    fun `a bank can reduce money to a different currency`() {
        val bank = Bank()
        bank.addRate(from = "CHF", to = "USD", rate = 2)
        val result = bank.reduce(Money.frank(2), "USD")
        assertThat(result, equalTo(Money.dollar(1)))
    }

    @Test fun `a bank can reduce a sum of moneys to a specific currency`() {
        val fiveBucks: Expression = Money.dollar(5)
        val tenFrancs: Expression = Money.frank(10)
        val bank = Bank()
        bank.addRate(from = "CHF", to = "USD", rate = 2)
        val mixCurrencySum = fiveBucks.plus(tenFrancs)
        val result = bank.reduce(mixCurrencySum, "USD")
        assertThat(result, equalTo(Money.dollar(10)))
    }

    @Test fun `two sum expressions can be summed up`() {
        val result = Sum(Money.dollar(5), Money.dollar(5))
                .plus(Sum(Money.dollar(5), Money.dollar(5)))
        assertThat(result.reduce(Bank(), "USD"), equalTo(Money.dollar(20)))
    }

    @Test fun `a bank can reduce a concatenated sum of money to a specific currency `() {
        val fiveBucks: Expression = Money.dollar(5)
        val tenFrancs: Expression = Money.frank(10)
        val bank = Bank()
        bank.addRate(from = "CHF", to = "USD", rate = 2)
        val concatenatedSum: Expression = Sum(fiveBucks, tenFrancs).plus(fiveBucks)
        val result = bank.reduce(concatenatedSum, "USD")
        assertThat(result, equalTo(Money.dollar(15)))
    }

    @Test fun `a sum can multiply`() {
        val fiveBucks: Expression = Money.dollar(5)
        val tenFrancs: Expression = Money.frank(10)
        val bank = Bank()
        bank.addRate("CHF", "USD", 2)
        val sum: Expression = Sum(fiveBucks, tenFrancs).times(2)
        val result: Money = bank.reduce(sum, "USD")
        assertThat(result, equalTo(Money.dollar(20)))
    }
}
