package com.zea.tdd

class Pair(val from: String, val to: String) {

    override fun equals(other: Any?): Boolean {
        val pair: Pair = other as Pair
        return from == pair.from && to == pair.to
    }

    override fun hashCode(): Int {
        return 0
    }
}
