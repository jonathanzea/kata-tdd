package com.zea.tdd

class Sum(val augend: Expression, val addend: Expression) : Expression {

    override fun reduce(bank: Bank, to: String): Money {
        val amount = bank.reduce(augend, to).amount + bank.reduce(addend, to).amount
        return Money(amount, to)
    }

    override fun plus(addend: Expression): Expression {
        return Sum(this, addend)
    }

    override fun times(multiplier: Int): Expression {
        return Sum(augend.times(multiplier), addend.times(multiplier))
    }
}
