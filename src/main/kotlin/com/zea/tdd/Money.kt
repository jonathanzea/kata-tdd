package com.zea.tdd

open class Money(var amount: Int, var currency: String) : Expression {

    companion object {
        fun dollar(amount: Int): Money {
            return Money(amount, "USD")
        }

        fun frank(amount: Int): Money {
            return Money(amount, "CHF")
        }
    }

    override fun times(multiplier: Int): Expression {
        return Money(amount * multiplier, currency)
    }

    override fun plus(addend: Expression): Expression {
        return Sum(this, addend)
    }

    override fun reduce(bank: Bank, to: String): Money {
        val rate = bank.rate(currency, to)
        return Money(amount / rate, to)
    }

    override fun equals(other: Any?): Boolean {
        val amountsAreEqual = amount == (other as Money).amount
        val currenciesAreEqual = other.currency == other.currency
        return amountsAreEqual && currenciesAreEqual
    }

    override fun toString(): String {
        return "$amount $currency"
    }
}
